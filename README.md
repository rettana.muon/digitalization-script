# Digitalization Script  
![](asset/digitalize.gif)

Javascript text animation themed on digital/glitched text.
npm 
## Installation  
Install the digtalize package.
```
npm install @abikebuk/digitalize
```
import the script.
@TODO

## Features
* Linear (from left to right) text display.
* Speed of animation customizable.
* Multiples modes : 
    * Linear
    * Full
    * Countdown

## Author  
Abikebuk (Rettan Muon) : <rettana.muon@gmail.com>
